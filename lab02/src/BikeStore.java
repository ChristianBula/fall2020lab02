// Oleg Christian Bula 1935095

public class BikeStore {
     public static void main(String args[]) {
    	Bicycle[] bicycles = new Bicycle[4];
    	bicycles[0] = new Bicycle("Pixel",16,40);
    	bicycles[1] = new Bicycle("AMD",16,35);
    	bicycles[2] = new Bicycle("Trek",8,40);
    	bicycles[3] = new Bicycle("Pinarello",10,20);
    	
    	for(int i = 0;i < bicycles.length;i++) {
    		System.out.println(bicycles[i].toString());
    	}
     }
}
